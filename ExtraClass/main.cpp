#include <iostream>
#include <fstream>
#include <sstream>

#include "Matrixnode.h"
#include "List.h"
#include "ClaseEjemplo.h"

using namespace std;

int getIndexYear(string year){
    switch(stoi(year)){
    case 1990:
        return 0;
        break;
    case 1991:
        return 1;
        break;
    case 1992:
        return 2;
        break;
    case 1993:
        return 3;
        break;
    case 1994:
        return 4;
        break;
    case 1995:
        return 5;
        break;
    case 1996:
        return 6;
        break;
    case 1997:
        return 7;
        break;
    case 1998:
        return 8;
        break;
    case 1999:
        return 9;
        break;
    case 2000:
        return 10;
        break;
    default:
        return -1;
    }
}

int main()
{
    //Variables string para la toma de datos
    string path = "";
    string data = "";
    string item = "";

    //Contador para manejo de posicion
    int counter = 0;

    //Creacion e inicialización de arreglo de 2 dimensiones de tipo Nodo para almacenar a las personas
    MatrixNode *matrix[11][12];
    for(int i=0; i<11; i++){
        for(int j=0; j<12; j++){
            matrix[i][j] = NULL;
        }
    }

    //Creación de lista
    DoubleList *people = new DoubleList();

    //Arreglo para la toma de valores
    string values[4];

    //Solicitud del path
    cout << "Buen dia visitante para comenzar le solicito que ingrese la dirección de su archivo de entrada" << endl;
    getline(cin,path);

    //Lectura del archivo
    ifstream file;
    file.open(path, ios::in);

    //Lectura de archivo
    if(!file.fail()){
        while(!file.eof()){
            getline(file, data);
            istringstream div(data);
            while(getline(div, item, ',')){
                values[counter] = item;
                counter++;
            }
            matrix[getIndexYear(values[0])][stoi(values[1])-1] = new MatrixNode(values[2], stoi(values[3]));
            counter =0;

        }
    }
    file.close();
    //Recorrido de la matriz
    /*
    for(int i=0; i<11; i++){
        for(int j=0; j<12; j++){
            if(matrix[i][j]!=NULL){
                cout<<matrix[i][j]->getNombre()<<endl;
            }
        }
    }*/

    //Insertar datos en lista
    counter = 0;
    for(int i=0; i<11; i++){
        for(int j=0; j<12; j++){
            if(matrix[i][j]!=NULL){
                people->insertList(matrix[i][j]->getNombre(), matrix[i][j]->getEdad(), (i*12+j));
                counter++;
            }
            else{
                people->insertList("", 0, (i*12+j));
            }

        }
    }

    people->getPersonRow(getIndexYear("1995"),(10-1));
    people->getPersonRow(getIndexYear("1992"),(6-1));


    Ejemplo *ejemplo1 = new Ejemplo("Carlos");
    ejemplo1->HolaMundo();

    return 0;
}


