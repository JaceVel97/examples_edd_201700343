#include "Node.h"

class DoubleList{
private:
    DoubleNode *First;
    DoubleNode *Last;

public:
    //Construct
    DoubleList();

    //Method
    int getSize();
    bool isEmpty();

    void getList();
    void getListReverse();

    void getPersonRow(int year, int month);

    void insertList(string Nombre, int edad, int index);
    void deleteValue(int value);
};
