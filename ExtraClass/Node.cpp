#include "Node.h"

DoubleNode::DoubleNode(){
    this->Nombre = "";
    this->edad = 0;
    this->index = 0;
    this->NextDouble = NULL;
    this->PreviousDouble = 0;
}

DoubleNode::DoubleNode(string Nombre, int edad_,int index, DoubleNode *nextNode_, DoubleNode *previousDouble_){
    this->Nombre = Nombre;
    this->edad = edad_;
    this->index = index;
    this->NextDouble = nextNode_;
    this->PreviousDouble = previousDouble_;
}

string DoubleNode::getNombre() {
    return this->Nombre;
}

int DoubleNode::getEdad(){
    return this->edad;
}

int DoubleNode::getIndex(){
    return this->index;
}

DoubleNode *DoubleNode::getNext(){
    return this->NextDouble;
}

DoubleNode *DoubleNode::getPrevious(){
    return this->PreviousDouble;
}

void DoubleNode::setNombre(string nombre){
    this->Nombre = nombre;
}

void DoubleNode::setEdad(int edad){
    this->edad = edad;
}

void DoubleNode::setIndex(int index){
    this->index = index;
}

void DoubleNode::setNext(DoubleNode *next){
    this->NextDouble = next;
}

void DoubleNode::setPrevious(DoubleNode *previous){
    this->PreviousDouble = previous;
}
