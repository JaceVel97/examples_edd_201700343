#include <string>
#include <iostream>

using namespace std;

class DoubleNode {
private:
    string Nombre;
    int edad;
    int index;
    DoubleNode *NextDouble;
    DoubleNode *PreviousDouble;

public:
    //Constructs
    DoubleNode();
    DoubleNode(string Nombre, int edad_,int index, DoubleNode *nextNode_, DoubleNode *previousDouble_);

    //Getters
    string getNombre();
    int getEdad();
    int getIndex();
    DoubleNode *getNext();
    DoubleNode *getPrevious();

    //Setters
    void setNombre(string nombre);
    void setEdad(int edad);
    void setIndex(int index);
    void setNext(DoubleNode *next);
    void setPrevious(DoubleNode *previous);
};
