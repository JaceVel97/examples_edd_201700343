#include "List.h"

DoubleList::DoubleList(){
    this->First = NULL;
    this->Last = 0;
}

int DoubleList::getSize(){
    DoubleNode *aux = First;
    int counter = 0;

    while(aux != NULL){
        counter++;
        aux = aux->getNext();
    }
    delete aux;
    return counter;
}

bool DoubleList::isEmpty(){
    return this->First == NULL;
}

void DoubleList::getList(){
    DoubleNode *aux = First;

    while(aux != NULL){
        cout<<aux->getNombre()<<" - "<<aux->getEdad()<<endl;
        aux = aux->getNext();
    }
    delete aux;
}

void DoubleList::getListReverse(){
    DoubleNode *aux = this->Last;

    while(aux != NULL){
        cout<<aux->getNombre()<<" - "<<aux->getEdad()<<endl;
        aux = aux->getPrevious();
    }
    delete aux;
}

void DoubleList::getPersonRow(int year, int month){
    DoubleNode *aux = First;

    while(aux != NULL){
        if(aux->getIndex() == (year*12+month)){
            cout<<aux->getNombre()<<" - "<<aux->getEdad()<<endl;
            break;
        }
        aux = aux->getNext();
    }
    delete aux;
}


void DoubleList::insertList(string Nombre, int edad, int index){
    DoubleNode *newNode = new DoubleNode(Nombre, edad, index, NULL, NULL);
    if(isEmpty()){
        this->First = newNode;
        this->Last = this->First;
    }
    else {
        this->Last->setNext(newNode);
        newNode->setPrevious(this->Last);
        this->Last = newNode;
    }
}
