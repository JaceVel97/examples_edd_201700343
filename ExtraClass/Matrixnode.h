#include <string>
#include <iostream>

using namespace std;

class MatrixNode {
private:
    string Nombre;
    int edad;

public:
    //Constructs
    MatrixNode();
    MatrixNode(string Nombre, int edad_);

    //Getters
    string getNombre();
    int getEdad();

    //Setters
    void setNombre(string nombre);
    void setEdad(int edad);
};
