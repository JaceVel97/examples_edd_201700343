#include "Matrixnode.h"

MatrixNode::MatrixNode(){
    this->Nombre = "";
    this->edad = 0;
}

MatrixNode::MatrixNode(string Nombre, int edad_){
    this->Nombre = Nombre;
    this->edad = edad_;
}

string MatrixNode::getNombre() {
    return this->Nombre;
}

int MatrixNode::getEdad(){
    return this->edad;
}

void MatrixNode::setNombre(string nombre){
    this->Nombre = nombre;
}

void MatrixNode::setEdad(int edad){
    this->edad = edad;
}

