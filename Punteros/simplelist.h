#include "simplenode.h"

class ListS{
private:
    Node *First;

public:
    //Construct
    ListS();

    //Methods
    bool isEmpty();
    int getSize();

    void getListSimple();

    void insertValue(int value);
    void insertValueReverse(int value);

    void deleteValue(int value);
};
