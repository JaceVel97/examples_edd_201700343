#include "list.h"

DoubleList::DoubleList(){
    this->First = NULL;
    this->Last = 0;
}

int DoubleList::getSize(){
    DoubleNode *aux = First;
    int counter = 0;

    while(aux != NULL){
        counter++;
        aux = aux->getNext();
    }
    delete aux;
    return counter;
}

bool DoubleList::isEmpty(){
    return this->First == NULL;
}

void DoubleList::getList(){
    DoubleNode *aux = First;

    while(aux != NULL){
        cout<<aux->getNombre()<<" - "<<aux->getEdad()<<endl;
        aux = aux->getNext();
    }
    delete aux;
}

void DoubleList::getListReverse(){
    DoubleNode *aux = this->Last;

    while(aux != NULL){
        cout<<aux->getNombre()<<" - "<<aux->getEdad()<<endl;
        aux = aux->getPrevious();
    }
    delete aux;
}


void DoubleList::insertList(string Nombre, int edad){
    DoubleNode *newNode = new DoubleNode(Nombre, edad, NULL, NULL);
    if(isEmpty()){
        this->First = newNode;
        this->Last = this->First;
    }
    else {
        this->Last->setNext(newNode);
        newNode->setPrevious(this->Last);
        this->Last = newNode;
    }
}
/*
void DoubleList::deleteValue(int value) {
    DoubleNode *aux = this->First;
    while(this->First != NULL){
        if(this->First->getValue() == value){
            if(aux == this->First){
                aux = this->First->getNext();
                aux->setPrevious(NULL);
                break;
            }
            else if (this->First == this->Last){
                this->Last = this->First->getPrevious();
                this->Last->setNext(NULL);
                break;
            }
            else {
                this->First->getNext()->setPrevious(this->First->getPrevious());
                this->First->getPrevious()->setNext(this->First->getNext());
                break;
            }
        }
        this->First = this->First->getNext();
    }
    this->First = aux;
}
*/
