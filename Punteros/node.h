#include <string>
#include <iostream>

using namespace std;

class DoubleNode {
private:
    string Nombre;
    int edad;
    DoubleNode *NextDouble;
    DoubleNode *PreviousDouble;

public:
    //Constructs
    DoubleNode();
    DoubleNode(string Nombre, int edad_, DoubleNode *nextNode_, DoubleNode *previousDouble_);

    //Getters
    string getNombre();
    int getEdad();
    DoubleNode *getNext();
    DoubleNode *getPrevious();

    //Setters
    void setNombre(string nombre);
    void setEdad(int edad);
    void setNext(DoubleNode *next);
    void setPrevious(DoubleNode *previous);
};
