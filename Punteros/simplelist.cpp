#include "simplelist.h"

ListS::ListS(){
    this->First = NULL;
}

bool ListS::isEmpty(){
    return this->First==NULL;
}

int ListS::getSize(){
    Node *aux = this->First;
    int counter = 0;
    while(aux != NULL){
        counter++;
        aux = aux->getNext();
    }
    return counter;
}

void ListS::getListSimple(){
    Node *aux = this->First;
    while(aux != NULL){
        cout<<"Valor: "<<aux->getValue()<<endl;
        aux = aux->getNext();
    }
}

void ListS::insertValueReverse(int value){
    Node *newNode = new Node(value);
    if(isEmpty()){
        this->First = newNode;
    }
    else{
        newNode->setNext(this->First);
        this->First = newNode;
    }
}


void ListS::insertValue(int value){
    Node *newNode = new Node(value);
    if(isEmpty()){
        this->First = newNode;
    }
    else{
        Node *aux = this->First;
        while(this->First->getNext() != NULL){
            this->First = this->First->getNext();
        }
        this->First->setNext(newNode);
        this->First = aux;
    }
}

