#include "simplenode.h"

//Constructs
Node::Node(){
    this->value = 0;
    this->Next = NULL;
}

Node::Node(int value_){
    this->value = value_;
    this->Next = 0;
}

Node::Node(int value_, Node *next_){
    this->value = value_;
    this->Next = next_;
}

//Getters
int Node::getValue(){
    return this->value;
}

Node *Node::getNext(){
    return this->Next;
}

//Setters
void Node::setValue(int value_){
    this->value = value_;
}

void Node::setNext(Node *next_){
    this->Next = next_;
}
