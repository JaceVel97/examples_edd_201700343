#include "node.h"

class DoubleList{
private:
    DoubleNode *First;
    DoubleNode *Last;

public:
    //Construct
    DoubleList();

    //Method
    int getSize();
    bool isEmpty();

    void getList();
    void getListReverse();

    void insertList(string Nombre, int edad);
    void deleteValue(int value);
};
