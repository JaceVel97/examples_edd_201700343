#include <string>
#include <iostream>

using namespace std;

class Node{
private:
    int value;
    Node *Next;
public:
    //Constructs
    Node();
    Node(int value_);
    Node(int value_, Node *next_);

    //Getters
    int getValue();
    Node *getNext();

    //Setters
    void setValue(int value_);
    void setNext(Node *next_);
};
