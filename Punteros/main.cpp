#include <iostream>
#include "list.h"
#include "simplelist.h"
using namespace std;

class Test{
    int valor;
    string nombre;
public:
    Test(){
        this->valor = 0;
        this->nombre = "";
    }

    void setValues(int valor_, string nombre_){
        this->valor = valor_;
        this->nombre = nombre_;
    }

    void print(){
        cout<<getNombre()<<endl;
        cout<<getValue()<<endl;
    }

    int getValue(){
        return this->valor;
    }

    string getNombre(){
        return this->nombre;
    }
};

int main()
{
    Test *test1 = new Test();
    test1->setValues(5, "Jose");
    test1->print();
    cout<<test1->getNombre()<<endl;
    cout << "Hello world!" << endl;
    int a = 21;
    int *b = &a;
    int **c = &b;
    a++;
    a = a*2;
    Node *ejemplo[2][2][2];
    ejemplo[0][0][0]= new Node();
    ejemplo[0][0][0]->setValue(57);
    cout<<"Valor ->"<<ejemplo[0][0][0]->getValue()<<endl;
    cout<<*b<<endl;
    cout<<**c<<endl;
    return 0;
}
