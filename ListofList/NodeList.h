#include <string>
#include <iostream>

#include "NodeList2.h"

using namespace std;

class NodeL {
private:
    string Nombre;
    string Carnet;
    NodeL *Next;
    NodeL *Previous;
    NodeE *Down;

public:
    //Constructs
    NodeL();
    NodeL(string nombre, string carnet, NodeL *nextNode_, NodeL *previousDouble_, NodeE *down);

    //Getters
    string getNombre();
    string getCarnet();
    NodeL *getNext();
    NodeL *getPrevious();
    NodeE *getDown();

    //Setters
    void setNombre(string nombre);
    void setCarnet(string Carnet);
    void setNext(NodeL *next);
    void setPrevious(NodeL *previous);
    void setDown(NodeE *down);
};
