#include "LList.h"

LList::LList(){
    this->First = 0;
    this->Last = 0;
}

int LList::getSize(){
    NodeL *aux = First;
    int counter = 0;

    while(aux != NULL){
        counter++;
        aux = aux->getNext();
    }
    delete aux;
    return counter;
}

bool LList::isEmpty(){
    return this->First == NULL;
}

void LList::getList(){
    NodeL *aux = First;

    while(aux != NULL){
        cout<<aux->getNombre()<<" - "<<aux->getCarnet()<<endl;
        aux = aux->getNext();
    }
    delete aux;
}

void LList::getListCourse(){
    NodeL *aux = First;

    while(aux != NULL){
        cout<<aux->getNombre()<<" - "<<aux->getCarnet()<<endl;
        NodeE *aux2 = aux->getDown();
        while(aux->getDown()!=NULL){
            cout<<"|---->"<<aux->getDown()->getCurso()<<" - "<< aux->getDown()->getCreditos()<<endl;
            aux->setDown(aux->getDown()->getDown());
        }
        aux->setDown(aux2);
        aux = aux->getNext();
    }
    delete aux;
}

void LList::getListReverse(){
    NodeL *aux = this->Last;

    while(aux != NULL){
        cout<<aux->getNombre()<<" - "<<aux->getCarnet()<<endl;
        aux = aux->getPrevious();
    }
    delete aux;
}


void LList::insertList(string Nombre, string carnet){
    NodeL *newNode = new NodeL(Nombre, carnet, NULL, NULL, NULL);
    if(isEmpty()){
        this->First = newNode;
        this->Last = this->First;
    }
    else {
        this->Last->setNext(newNode);
        newNode->setPrevious(this->Last);
        this->Last = newNode;
    }
}

void LList::insetCourse(string Carnet, string Course, int Creditos){
    NodeL *aux = this->First;
    bool verification = false;

    while(this->First != NULL){
        if(this->First->getCarnet() == Carnet){
            NodeE *newNode = new NodeE(Course, Creditos, NULL, NULL);
            if(this->First->getDown() == NULL){
                this->First->setDown(newNode);
            }
            else{
                NodeE *aux2 = this->First->getDown();
                while(this->First->getDown()->getDown() != NULL){
                    this->First->setDown(this->First->getDown()->getDown());
                }
                this->First->getDown()->setDown(newNode);
                this->First->setDown(aux2);
            }
            verification = true;
        }
        this->First = this->First->getNext();
    }
    if(!verification){
        cout<<"No existe un alumno con este el carn�: "<<Carnet<<endl;
    }
    this->First = aux;
}

