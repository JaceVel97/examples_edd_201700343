#include "NodeList2.h"

NodeE::NodeE(){
    this->Curso = "";
    this->Creditos = 0;
    this->Up = 0;
    this->Down = 0;
}

NodeE::NodeE(string Curso, int Creditos, NodeE *Up, NodeE *down){
    this->Curso = Curso;
    this->Creditos = Creditos;
    this->Up = Up;
    this->Down = down;
}

string NodeE::getCurso() {
    return this->Curso;
}

int NodeE::getCreditos(){
    return this->Creditos;
}

NodeE *NodeE::getUp(){
    return this->Up;
}

NodeE *NodeE::getDown(){
return this->Down;
}

void NodeE::setCurso(string Curso){
    this->Curso = Curso;
}
void NodeE::setCreditos(int Creditos){
    this->Creditos = Creditos;
}

void NodeE::setUp(NodeE *Up){
    this->Up = Up;
}

void NodeE::setDown(NodeE *down){
    this->Down = down;
}
