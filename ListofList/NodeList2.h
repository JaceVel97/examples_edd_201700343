#include <string>
#include <iostream>

using namespace std;

class NodeE {
private:
    string Curso;
    int Creditos;
    NodeE *Up;
    NodeE *Down;

public:
    //Constructs
    NodeE();
    NodeE(string curso, int creditos, NodeE *upNode, NodeE *downNode);

    //Getters
    string getCurso();
    int getCreditos();
    NodeE *getUp();
    NodeE *getDown();

    //Setters
    void setCurso(string curso);
    void setCreditos(int creditos);
    void setUp(NodeE *up);
    void setDown(NodeE *down);
};
