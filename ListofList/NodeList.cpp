#include "NodeList.h"

NodeL::NodeL(){
    this->Nombre = "";
    this->Carnet = "";
    this->Next = NULL;
    this->Previous = 0;
    this->Down = 0;
}

NodeL::NodeL(string Nombre, string Carnet, NodeL *nextNode_, NodeL *previousDouble_, NodeE *down){
    this->Nombre = Nombre;
    this->Carnet = Carnet;
    this->Next = nextNode_;
    this->Previous = previousDouble_;
    this->Down = down;
}

string NodeL::getNombre() {
    return this->Nombre;
}

string NodeL::getCarnet(){
    return this->Carnet;
}

NodeL *NodeL::getNext(){
    return this->Next;
}

NodeL *NodeL::getPrevious(){
    return this->Previous;
}

NodeE *NodeL::getDown(){
return this->Down;
}

void NodeL::setNombre(string nombre){
    this->Nombre = nombre;
}
void NodeL::setCarnet(string Carnet){
    this->Carnet = Carnet;
}

void NodeL::setNext(NodeL *next){
    this->Next = next;
}

void NodeL::setPrevious(NodeL *previous){
    this->Previous = previous;
}

void NodeL::setDown(NodeE *down){
    this->Down = down;
}
