#include "NodeList.h"

class LList{
private:
    NodeL *First;
    NodeL *Last;

public:
    //Construct
    LList();

    //Method
    int getSize();
    bool isEmpty();

    void getList();
    void getListCourse();
    void getListReverse();

    void insertList(string Nombre, string Carnet);

    void insetCourse(string Carnet, string Course, int Creditos);
};
