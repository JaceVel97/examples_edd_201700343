#include <string>
#include <iostream>

using namespace std;

class DoubleNode {
private:
    string Nombre;
    DoubleNode *NextDouble;
    DoubleNode *PreviousDouble;

public:
    //Constructs
    DoubleNode();
    DoubleNode(string Nombre, DoubleNode *nextNode_, DoubleNode *previousDouble_);

    //Getters
    string getNombre();
    DoubleNode *getNext();
    DoubleNode *getPrevious();

    //Setters
    void setNombre(string nombre);
    void setNext(DoubleNode *next);
    void setPrevious(DoubleNode *previous);
};

