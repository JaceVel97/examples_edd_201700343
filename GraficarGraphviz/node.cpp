    #include "node.h"

    DoubleNode::DoubleNode(){
        this->Nombre = "";
        this->NextDouble = NULL;
        this->PreviousDouble = 0;
    }

    DoubleNode::DoubleNode(string Nombre, DoubleNode *nextNode_, DoubleNode *previousDouble_){
        this->Nombre = Nombre;
        this->NextDouble = nextNode_;
        this->PreviousDouble = previousDouble_;
    }

    string DoubleNode::getNombre() {
        return this->Nombre;
    }

    DoubleNode *DoubleNode::getNext(){
        return this->NextDouble;
    }

    DoubleNode *DoubleNode::getPrevious(){
        return this->PreviousDouble;
    }

    void DoubleNode::setNombre(string nombre){
        this->Nombre = nombre;
    }

    void DoubleNode::setNext(DoubleNode *next){
        this->NextDouble = next;
    }

    void DoubleNode::setPrevious(DoubleNode *previous){
        this->PreviousDouble = previous;
    }
