#include "list.h"
#include <fstream>

DoubleList::DoubleList(){
    this->First = NULL;
    this->Last = 0;
}

int DoubleList::getSize(){
    DoubleNode *aux = First;
    int counter = 0;

    while(aux != NULL){
        counter++;
        aux = aux->getNext();
    }
    delete aux;
    return counter;
}

bool DoubleList::isEmpty(){
    return this->First == NULL;
}

void DoubleList::getList(){
    DoubleNode *aux = First;

    while(aux != NULL){
        cout<<aux->getNombre()<<endl;
        aux = aux->getNext();
    }
    delete aux;
}

void DoubleList::getListReverse(){
    DoubleNode *aux = this->Last;

    while(aux != NULL){
        cout<<aux->getNombre()<<endl;
        aux = aux->getPrevious();
    }
    delete aux;
}


void DoubleList::getGraph(){
    DoubleNode *aux = First;
    string node_data = "";
    string edge_data = "";
    string graph = "digraph List {\nrankdir=LR;\nnode [shape = record, color=blue , style=filled, fillcolor=skyblue];\n";
    int counter = 0;
    while(aux != NULL){
        cout<<aux->getNombre()<<endl;
        node_data += "Node" + to_string(counter) + "[label=\"" + aux->getNombre() + "\"];\n";
        if(aux->getPrevious()!=NULL){
            edge_data += "Node" + to_string(counter-1) + "->Node" + to_string(counter) + ";\n";
            edge_data += "Node" + to_string(counter) + "->Node" + to_string(counter-1) + ";\n";
        }
        counter++;
        aux = aux->getNext();
    }
    graph += node_data;
    graph += edge_data;
    graph += "\n}";
    //-------------------------------------
    try{
        //Esta variable debe ser modificada para agregar su path de creacion de la Grafica
        string path = "Path_a_graficar";

        ofstream file;
        file.open(path + "Graph.dot",std::ios::out);

        if(file.fail()){
            exit(1);
        }
        file<<graph;
        file.close();
        string command = "dot -Tpng " + path + "Graph.dot -o  " + path + "Graph.png";
        system(command.c_str());

    }catch(exception e){
        cout<<"Fallo detectado"<<endl;
    }
    //-------------------------------------

    delete aux;
}

void DoubleList::insertList(string Nombre){
    DoubleNode *newNode = new DoubleNode(Nombre, NULL, NULL);
    if(isEmpty()){
        this->First = newNode;
        this->Last = this->First;
    }
    else {
        this->Last->setNext(newNode);
        newNode->setPrevious(this->Last);
        this->Last = newNode;
    }
}
