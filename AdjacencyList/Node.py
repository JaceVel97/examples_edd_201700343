class Node:
    def __init__(self, number):
        self.number = number
        self.weight = 0
        self.Next = None
        self.Previous = None


class NodeGraph:
    def __init__(self, number, list_):
        self.number = number
        self.list = list_
        self.Next = None
        self.Previous = None
