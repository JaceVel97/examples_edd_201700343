from List import List
from Node import NodeGraph


class AdjacencyList:
    def __init__(self):
        self.First = None
        self.Last = None

    def get_size(self):
        aux = self.First
        counter = 0
        while aux is not None:
            counter += 1
            aux = aux.Next
        return counter

    def is_empty(self):
        return self.First is None

    def get_list(self):
        aux = self.First
        counter = 0
        adjacency_list = ""
        while aux is not None:
            if not aux.list.is_empty():
                aux2 = aux.list.First
                while aux2 is not None:
                    adjacency_list += " ->" + str(aux2.number)
                    aux2 = aux2.Next
            print(str(counter) + ") " + str(aux.number) + ":" + adjacency_list)
            adjacency_list = ""
            counter += 1
            aux = aux.Next

    def exists(self, verification_number):
        aux = self.First
        while aux is not None:
            if aux.number == verification_number:
                return True
            aux = aux.Next
        return False

    def insert_node(self, number):
        if not self.exists(number):
            new_list = List()
            new_node = NodeGraph(number, new_list)

            if self.is_empty():
                self.Last = new_node
                self.First = self.Last
            else:
                self.Last.Next = new_node
                new_node.Previous = self.Last
                self.Last = new_node
        else:
            print("El valor ya existe dentro de la lista")

    def link_graph(self, value1, value2):
        aux = self.First
        while aux is not None:
            if aux.number == value1:
                aux.list.insert_value(value2)
                break
            aux = aux.Next

        # while aux is not None:
        #     if aux.number == value2:
        #         aux.list.insert_value(value1)
        #         break
        #     aux = aux.Next

    def depth(self, initial_node, final_node):
        list = [initial_node]
        result_list = []
        aux_print = ""
        while list:
            actual_node = list.pop(0)
            result_list.append(actual_node)
            if actual_node == final_node:
                for i in range(len(result_list)):
                    if i < len(result_list)-1:
                        aux_print += str(result_list[i]) + '->'
                    else:
                        aux_print += str(result_list[i])
                print(aux_print)
                return print("Se ha encontrado la solución")

            temp = self.successor(actual_node)
            temp.reverse()
            if temp:
                temp.extend(list)
                list = temp
                #print(list)
        print("No hay solución")

    def breadth(self, initial_node, final_node):
        list = [initial_node]
        result_list = []
        aux_print = ""
        while list:
            actual_node = list.pop(0)
            result_list.append(actual_node)
            if actual_node == final_node:
                for i in range(len(result_list)):
                    if i < len(result_list) - 1:
                        aux_print += str(result_list[i]) + '->'
                    else:
                        aux_print += str(result_list[i])
                print(aux_print)
                return print("Se ha encontrado la solución")

            temp = self.successor(actual_node)

            if temp:
                list.extend(temp)
                # print(list)
        print("No hay solución")

    def successor(self, node):
        aux = self.First
        while aux is not None:
            if aux.number == node:
                return aux.list.get_list()
            aux = aux.Next
