from AdjacencyList import AdjacencyList

new_al = AdjacencyList()

new_al.insert_node(1)
new_al.insert_node(2)
new_al.insert_node(3)
new_al.insert_node(4)
new_al.insert_node(5)
new_al.insert_node(6)

new_al.link_graph(1, 2)
new_al.link_graph(1, 3)
new_al.link_graph(2, 4)
new_al.link_graph(3, 5)
new_al.link_graph(4, 6)
new_al.link_graph(5, 6)

new_al.get_list()

print('---------------')
new_al.depth(1, 6)
print('---------------')
new_al.breadth(1, 6)