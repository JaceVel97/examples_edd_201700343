
# parsetab.py
# This file is automatically generated. Do not edit.
# pylint: disable=W,C,R
_tabversion = '3.10'

_lr_method = 'LALR'

_lr_signature = 'DATE DOLAR EQUALS HOUR ID LQUESTION NORMSTRING NUMBER QUOTATION_MARKS RQUESTION TCARNET TCARRERA TCREDITOS TDESCRIPCION TDPI TEDAD TELEMENT TELEMENTS TESTADO TFECHA THORA TITEM TMATERIA TNOMBRE TPASSWORD TTASK TTYPE TUSERstatement : LQUESTION TELEMENTS RQUESTION elementos LQUESTION DOLAR TELEMENTS RQUESTIONelementos : elementos elemento\n                 | elemento\n    elemento : LQUESTION TELEMENT  tipoElemento RQUESTION items LQUESTION DOLAR TELEMENT RQUESTIONtipoElemento : TTYPE EQUALS QUOTATION_MARKS TUSER QUOTATION_MARKS\n                    | TTYPE EQUALS QUOTATION_MARKS TTASK QUOTATION_MARKS\n    items : items item\n             | item\n    item : LQUESTION TITEM tipeItem EQUALS valueItem DOLAR RQUESTION\n    valueItem : NORMSTRING\n                 | NUMBER\n                 | DATE\n                 | HOUR\n                 tipeItem : TCARNET\n                | TDPI\n                | TNOMBRE\n                | TCARRERA\n                | TPASSWORD\n                | TCREDITOS\n                | TEDAD\n                | TDESCRIPCION\n                | TMATERIA\n                | TFECHA\n                | THORA\n                | TESTADO\n                '
    
_lr_action_items = {'LQUESTION':([0,4,6,7,10,14,18,19,24,50,52,],[2,5,9,-3,-2,17,23,-8,-7,-4,-9,]),'$end':([1,21,],[0,-1,]),'TELEMENTS':([2,13,],[3,16,]),'RQUESTION':([3,11,16,41,42,44,51,],[4,14,21,-5,-6,50,52,]),'TELEMENT':([5,9,40,],[8,8,44,]),'TTYPE':([8,],[12,]),'DOLAR':([9,23,45,46,47,48,49,],[13,40,51,-10,-11,-12,-13,]),'EQUALS':([12,27,28,29,30,31,32,33,34,35,36,37,38,39,],[15,43,-14,-15,-16,-17,-18,-19,-20,-21,-22,-23,-24,-25,]),'QUOTATION_MARKS':([15,25,26,],[20,41,42,]),'TITEM':([17,23,],[22,22,]),'TUSER':([20,],[25,]),'TTASK':([20,],[26,]),'TCARNET':([22,],[28,]),'TDPI':([22,],[29,]),'TNOMBRE':([22,],[30,]),'TCARRERA':([22,],[31,]),'TPASSWORD':([22,],[32,]),'TCREDITOS':([22,],[33,]),'TEDAD':([22,],[34,]),'TDESCRIPCION':([22,],[35,]),'TMATERIA':([22,],[36,]),'TFECHA':([22,],[37,]),'THORA':([22,],[38,]),'TESTADO':([22,],[39,]),'NORMSTRING':([43,],[46,]),'NUMBER':([43,],[47,]),'DATE':([43,],[48,]),'HOUR':([43,],[49,]),}

_lr_action = {}
for _k, _v in _lr_action_items.items():
   for _x,_y in zip(_v[0],_v[1]):
      if not _x in _lr_action:  _lr_action[_x] = {}
      _lr_action[_x][_k] = _y
del _lr_action_items

_lr_goto_items = {'statement':([0,],[1,]),'elementos':([4,],[6,]),'elemento':([4,6,],[7,10,]),'tipoElemento':([8,],[11,]),'items':([14,],[18,]),'item':([14,18,],[19,24,]),'tipeItem':([22,],[27,]),'valueItem':([43,],[45,]),}

_lr_goto = {}
for _k, _v in _lr_goto_items.items():
   for _x, _y in zip(_v[0], _v[1]):
       if not _x in _lr_goto: _lr_goto[_x] = {}
       _lr_goto[_x][_k] = _y
del _lr_goto_items
_lr_productions = [
  ("S' -> statement","S'",1,None,None,None),
  ('statement -> LQUESTION TELEMENTS RQUESTION elementos LQUESTION DOLAR TELEMENTS RQUESTION','statement',8,'p_statement_group','Syntactic.py',8),
  ('elementos -> elementos elemento','elementos',2,'p_elementos_group','Syntactic.py',12),
  ('elementos -> elemento','elementos',1,'p_elementos_group','Syntactic.py',13),
  ('elemento -> LQUESTION TELEMENT tipoElemento RQUESTION items LQUESTION DOLAR TELEMENT RQUESTION','elemento',9,'p_elemento','Syntactic.py',23),
  ('tipoElemento -> TTYPE EQUALS QUOTATION_MARKS TUSER QUOTATION_MARKS','tipoElemento',5,'p_tipoElemento','Syntactic.py',27),
  ('tipoElemento -> TTYPE EQUALS QUOTATION_MARKS TTASK QUOTATION_MARKS','tipoElemento',5,'p_tipoElemento','Syntactic.py',28),
  ('items -> items item','items',2,'p_items','Syntactic.py',32),
  ('items -> item','items',1,'p_items','Syntactic.py',33),
  ('item -> LQUESTION TITEM tipeItem EQUALS valueItem DOLAR RQUESTION','item',7,'p_item','Syntactic.py',36),
  ('valueItem -> NORMSTRING','valueItem',1,'p_valueItem','Syntactic.py',40),
  ('valueItem -> NUMBER','valueItem',1,'p_valueItem','Syntactic.py',41),
  ('valueItem -> DATE','valueItem',1,'p_valueItem','Syntactic.py',42),
  ('valueItem -> HOUR','valueItem',1,'p_valueItem','Syntactic.py',43),
  ('tipeItem -> TCARNET','tipeItem',1,'p_tipeItem','Syntactic.py',46),
  ('tipeItem -> TDPI','tipeItem',1,'p_tipeItem','Syntactic.py',47),
  ('tipeItem -> TNOMBRE','tipeItem',1,'p_tipeItem','Syntactic.py',48),
  ('tipeItem -> TCARRERA','tipeItem',1,'p_tipeItem','Syntactic.py',49),
  ('tipeItem -> TPASSWORD','tipeItem',1,'p_tipeItem','Syntactic.py',50),
  ('tipeItem -> TCREDITOS','tipeItem',1,'p_tipeItem','Syntactic.py',51),
  ('tipeItem -> TEDAD','tipeItem',1,'p_tipeItem','Syntactic.py',52),
  ('tipeItem -> TDESCRIPCION','tipeItem',1,'p_tipeItem','Syntactic.py',53),
  ('tipeItem -> TMATERIA','tipeItem',1,'p_tipeItem','Syntactic.py',54),
  ('tipeItem -> TFECHA','tipeItem',1,'p_tipeItem','Syntactic.py',55),
  ('tipeItem -> THORA','tipeItem',1,'p_tipeItem','Syntactic.py',56),
  ('tipeItem -> TESTADO','tipeItem',1,'p_tipeItem','Syntactic.py',57),
]
