/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carlo
 */
import java.io.FileWriter;
import java.io.PrintWriter;

public class BTree {
    //Ra�z del arbol B, primer elemento
    PaginaB Raiz;
    //Informaci�n que se insertara dentro del arbol B
    String Codigo;
    String Pais;
    //Auxiliares que me ayudaran adelante en el proceso de inserciones
    boolean auxiliar1=false;
    PaginaB Auxliar2;
    boolean subeArriba=false;
    boolean estado=false;
    boolean comparador=false;
    //Variables que me sirven en el momento de graficar el �rbol B;
    
    String grafica;
    private String grafica2;
    String pais;
    int nodos;
    
    
    public boolean estaVacio(PaginaB raiz){
        return (raiz==null || raiz.getCuenta()==0);
    }
    
    public void InsertarDatos(String codigo,String pais){
        InsertarDatos2(Raiz,codigo,pais);
    }
    
    
    private void InsertarDatos2(PaginaB raiz,String codigo,String pais){
        Empujar(raiz,codigo,pais);
        if(subeArriba){
            Raiz=new PaginaB();
            Raiz.setCuenta(1);
            Raiz.setCodigo(0, this.Codigo);
            Raiz.setPais(0, this.Pais);
            
            Raiz.setApuntador(0, raiz);
            Raiz.setApuntador(1, this.Auxliar2);
        }
        
    }
    
    private void Empujar(PaginaB raiz,String codigo,String pais){
        int posicion=0;
        estado=false;
        
        if(estaVacio(raiz) && comparador==false){
            subeArriba=true;
            
            this.Codigo=codigo;
            this.Pais=pais;
            
            this.Auxliar2=null;
            
        }
        else {
            posicion=BuscarNodoB(codigo,raiz);
            if(comparador==false){
                if(estado){
                    subeArriba=false;
                }
                else {
                    Empujar(raiz.getApuntador(posicion),codigo,pais);
                
                    if(subeArriba){
                        if(raiz.getCuenta()<4){
                            subeArriba=false;
                            MeterHoja(raiz,posicion,this.Codigo,this.Pais);
                        }
                        else {
                            subeArriba=true;
                            DividirPaginaB(raiz,posicion,this.Codigo,this.Pais);
                        }
                    }
                }
                
            }
            else {
                System.out.println("Dato repetido"+ codigo);
                comparador=false;
            }
            
        }
    }
    
    private int BuscarNodoB(String codigo,PaginaB raiz){
        
        int auxContador=0;
       
        if(codigo.compareTo(raiz.getCodigo(0))<0){
            estado=false;
            
            auxContador=0;
        }
        else {
            while(auxContador!=raiz.getCuenta()){
                if(codigo==raiz.getCodigo(auxContador)){
                    comparador=true;
                }
                auxContador++;
            }
            auxContador=raiz.getCuenta();
            
            while(codigo.compareTo(raiz.getCodigo(auxContador-1))<0 && auxContador>1){
                --auxContador;
                
                estado=(codigo==raiz.getCodigo(auxContador-1)) ? true:false;
                                

            }
        }
        
        return auxContador;
    }
    
    public void MeterHoja(PaginaB raiz,int posicion,String codigo,String pais){
        int auxC=raiz.getCuenta();
        
        while(auxC!=posicion){
            if(auxC!=0){
                raiz.setCodigo(auxC, raiz.getCodigo(auxC-1));
                raiz.setPais(auxC, raiz.getPais(auxC-1));
                raiz.setApuntador(auxC+1, raiz.getApuntador(auxC));
            }
            auxC--;
        }
        
        raiz.setCodigo(posicion, codigo);
        raiz.setPais(posicion, pais);
        raiz.setApuntador(posicion+1, this.Auxliar2);
        raiz.setCuenta(raiz.getCuenta()+1);
    }
    
    public void DividirPaginaB(PaginaB raiz,int posicion,String codigo,String pais){
        int posicion2=0;
        int posicionMedia=0;
        
        if(posicion<=2){
            posicionMedia=2;
        }
        else {
            posicionMedia=3;
        }
        
        PaginaB paginaDerecha=new PaginaB();
        posicion2=posicionMedia+1;
        
        while(posicion2!=5){
            
            if((posicion2-posicionMedia)!=0){
                paginaDerecha.setCodigo((posicion2-posicionMedia)-1, raiz.getCodigo(posicion2-1));
                paginaDerecha.setPais((posicion2-posicionMedia)-1, raiz.getPais(posicion2-1));
                paginaDerecha.setApuntador(posicion2-posicionMedia, raiz.getApuntador(posicion2));
            }
            posicion2++;
        }
        
        paginaDerecha.setCuenta(4-posicionMedia);
        raiz.setCuenta(posicionMedia);
        
        if(posicion<=2){
            this.auxiliar1=true;
            MeterHoja(raiz,posicion,codigo,pais);
        }
        else{
            this.auxiliar1=true;
            MeterHoja(paginaDerecha,(posicion-posicionMedia),codigo,pais);
            
        }
        
        this.Codigo=raiz.getCodigo(raiz.getCuenta()-1);
        this.Pais=raiz.getPais(raiz.getCuenta()-1);
        
        paginaDerecha.setApuntador(0, raiz.getApuntador(raiz.getCuenta()));
        
        raiz.setCuenta(raiz.getCuenta()-1);
        this.Auxliar2=paginaDerecha;
        
        if(this.auxiliar1){
            raiz.setCodigo(3, "");
            raiz.setPais(3, "");
            raiz.setApuntador(4, null);
            
            raiz.setCodigo(2, "");
            raiz.setPais(2, "");
            raiz.setApuntador(3, null);
            
        }
        
    }
    
    public void  Preorden(){
        Preorden2(Raiz);
    }
    
    public void Preorden2(PaginaB pagina){
        if(pagina!=null){
            
            for(int i=0;i<pagina.getCuenta();i++){
                if(pagina.getCodigo(i)!=null){
                    if(pagina.getCodigo(i)!=""){
                        System.out.print(pagina.getCodigo(i)+"_");
                    }
                }
            }
            
            System.out.println("");
            
            Preorden2(pagina.getApuntador(0));
            Preorden2(pagina.getApuntador(1));
            Preorden2(pagina.getApuntador(2));
            Preorden2(pagina.getApuntador(3));
            Preorden2(pagina.getApuntador(4));
        }
    }
    
    public void Graficar(){
        
        grafica="digraph ArbolB{\n";
        grafica+="\nrankdir=TB;\n";
        grafica+="node[color=\"blue\",style=\"rounded,filled\",fillcolor=lightgray, shape=record];\n";
        //Declaracion de nodos 
        Graficar2(this.Raiz);
        //Declaraciones de relaciones entre nodos
        Graficar3(this.Raiz);
        
        grafica+="\n}\n";
        
        FileWriter fichero=null;
        PrintWriter pw=null;
        try {
            fichero=new FileWriter("C:\\Users\\carlo\\Desktop\\ArbolB.txt");
            pw=new PrintWriter(fichero);
    
            pw.print(grafica);
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(null!=fichero)
                    fichero.close();
            }catch(Exception e2){
                e2.printStackTrace();
            }
        }

        String direccionI="C:\\Program Files\\Graphviz\\bin\\dot.exe";
        String direccionArchivo="C:\\Users\\carlo\\Desktop\\ArbolB.txt";
        String direccionImagen="C:\\Users\\carlo\\Desktop\\ArbolB.png";
        String tParam="-Tpng";
        String tOParam="-o";

        String[] cmd=new String[5];
        try{
            cmd[0]=direccionI;
            cmd[1]=tParam;
            cmd[2]=direccionArchivo;
            cmd[3]=tOParam;
            cmd[4]=direccionImagen;

            Runtime rt=Runtime.getRuntime();
            rt.exec(cmd);


        }catch(Exception ex){
            ex.printStackTrace();

        }finally{
    
        }
    }
    
    private void Graficar2(PaginaB pagina){
        int contador=0;
         if(pagina!=null){
            this.nodos=0;
            for(int i=0;i<pagina.getCuenta();i++){
                
                if(pagina.getCodigo(i)!=null){
                    if(pagina.getCodigo(i)!=""){
                        this.nodos++;
                        if(i!=0){
                            grafica+="|";
                        }
                        if(this.nodos==1){
                            grafica+="\nNodo"+pagina.getCodigo(i)+"[label=\"<f0> |";
                        }
                        
                        if(i==0){
                            grafica+="<f"+(i+1)+">"+pagina.getCodigo(i)+"\\n"+pagina.getPais(i) + "|<f"+(i+2)+"> ";
                            contador=3;
                        }
                        else {
                            
                            grafica+="<f"+(contador)+">"+pagina.getCodigo(i)+"\\n"+pagina.getPais(i) + "|<f"+(contador+1)+"> ";
                            contador+=2;
                        }
                        
                        
                        if(i==pagina.getCuenta()-1){
                            contador=0;
                            grafica+=" \",group=0];\n";
                        }
                    }
                }
            }
            
            
            
            Graficar2(pagina.getApuntador(0));
            Graficar2(pagina.getApuntador(1));
            Graficar2(pagina.getApuntador(2));
            Graficar2(pagina.getApuntador(3));
            Graficar2(pagina.getApuntador(4));
        }
    }
    
    private void Graficar3(PaginaB pagina){
        if(pagina!=null){
            
            
                if(pagina.getCodigo(0)!=null){
                    if(pagina.getCodigo(0)!=""){
                        if(pagina.getApuntador(0)!=null && pagina.getApuntador(0).getCodigo(0)!=null){
                                grafica+="\nNodo"+pagina.getCodigo(0)+":f0->"+"Nodo"+pagina.getApuntador(0).getCodigo(0);
                            }
                           if(pagina.getApuntador(1)!=null && pagina.getApuntador(1).getCodigo(0)!=null){
                                grafica+="\nNodo"+pagina.getCodigo(0)+":f2->"+"Nodo"+pagina.getApuntador(1).getCodigo(0);
                           }
                           if(pagina.getApuntador(2)!=null && pagina.getApuntador(2).getCodigo(0)!=null){
                                grafica+="\nNodo"+pagina.getCodigo(0)+":f4->"+"Nodo"+pagina.getApuntador(2).getCodigo(0);
                           }
                           if(pagina.getApuntador(3)!=null && pagina.getApuntador(3).getCodigo(0)!=null){
                                grafica+="\nNodo"+pagina.getCodigo(0)+":f6->"+"Nodo"+pagina.getApuntador(3).getCodigo(0);
                           }
                           if(pagina.getApuntador(4)!=null && pagina.getApuntador(4).getCodigo(0)!=null){
                                grafica+="\nNodo"+pagina.getCodigo(0)+":f8->"+"Nodo"+pagina.getApuntador(4).getCodigo(0);
                           }
                    }
                }
            
            
           
            
            Graficar3(pagina.getApuntador(0));
            Graficar3(pagina.getApuntador(1));
            Graficar3(pagina.getApuntador(2));
            Graficar3(pagina.getApuntador(3));
            Graficar3(pagina.getApuntador(4));
    }
        
        
}
    public String getPaisM1(String codigo){
        String paisB;
        paisB="";
        getPaisM(this.Raiz,codigo);
        paisB=pais;
        pais="";
        return paisB;
    }
    public void getPaisM(PaginaB pagina,String codigo){
        
        if(pagina!=null){
            
            
               for(int i=0;i<pagina.getCuenta();i++){
                if(pagina.getCodigo(i)!=null){
                    if(pagina.getCodigo(i)!=""){
                        if(pagina.getCodigo(i)==(codigo)){
                            pais= pagina.getPais(i);
                            
                        }
                    }
                }
            }
            
            
           
            
            getPaisM(pagina.getApuntador(0),codigo);
            getPaisM(pagina.getApuntador(1),codigo);
            getPaisM(pagina.getApuntador(2),codigo);
            getPaisM(pagina.getApuntador(3),codigo);
            getPaisM(pagina.getApuntador(4),codigo);
            
            
        
        }
        
    }
}
