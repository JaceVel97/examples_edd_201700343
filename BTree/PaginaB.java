/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carlo
 */
public class PaginaB {
    public int Cuenta;
    public int maxClaves;
    public ListaPuntero punteros=new ListaPuntero();
    public ListaDoble datos=new ListaDoble();

    public PaginaB() {
        for(int i=0;i<5;i++){
            if(i!=4){
                datos.InsertarNodoD("", null);
            }
            punteros.InsertarPuntero(null);
        }
        maxClaves=5;
        
    }

    public boolean paginaLlena(){
        return(Cuenta==maxClaves-1);
    }
    public boolean paginaCasiLLena(){
        return(Cuenta==maxClaves/2);
    }
    
    public int getCuenta() {
        return Cuenta;
    }

    public void setCuenta(int Cuenta) {
        this.Cuenta = Cuenta;
    }

    public int getMaxClaves() {
        return maxClaves;
    }

    public void setMaxClaves(int maxClaves) {
        this.maxClaves = maxClaves;
    }
    
    public String getCodigo(int posicion){
        return datos.DevolverDato(posicion).getCodigo();
    }
    
    public void setCodigo(int posicion,String codigo){
        datos.InsertarDato(codigo, posicion);
    }
    
    public String getPais(int posicion){
        return datos.DevolverDato(posicion).getPais();
    }
    
    public void setPais(int posicion,String pais){
        datos.DevolverDato(posicion).setPais(pais);
    }
    
    public PaginaB getApuntador(int posicion){
        return punteros.DevolverPuntero(posicion).getPuntero();
    }
    
    public void setApuntador(int posicion,PaginaB puntero){
        punteros.InsertarPunteroP(puntero, posicion);
    }
    
}

