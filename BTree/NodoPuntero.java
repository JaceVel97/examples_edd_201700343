/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carlo
 */
public class NodoPuntero {
    private PaginaB Puntero;
    private NodoPuntero SiguienteP;
    private NodoPuntero AnteriorP;

    public NodoPuntero(PaginaB puntero) {
        this.Puntero=puntero;
        this.SiguienteP=null;
    }

    public PaginaB getPuntero() {
        return Puntero;
    }

    public void setPuntero(PaginaB Puntero) {
        this.Puntero = Puntero;
    }

    public NodoPuntero getSiguienteP() {
        return SiguienteP;
    }

    public void setSiguienteP(NodoPuntero SiguienteP) {
        this.SiguienteP = SiguienteP;
    }

    public NodoPuntero getAnteriorP() {
        return AnteriorP;
    }

    public void setAnteriorP(NodoPuntero AnteriorP) {
        this.AnteriorP = AnteriorP;
    }
    
}
