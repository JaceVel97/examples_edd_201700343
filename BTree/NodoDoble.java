/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carlo
 */
public class NodoDoble {
    private String codigo;
    private String pais;
    NodoDoble Siguiente;
    NodoDoble Anterior;

    public NodoDoble(String codigo, String pais) {
        this.codigo = codigo;
        this.pais=pais;
        this.Siguiente=null;
        this.Anterior=null;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
    
    public NodoDoble getSiguiente() {
        return Siguiente;
    }

    public void setSiguiente(NodoDoble Siguiente) {
        this.Siguiente = Siguiente;
    }

    public NodoDoble getAnterior() {
        return Anterior;
    }

    public void setAnterior(NodoDoble Anterior) {
        this.Anterior = Anterior;
    }
    
}