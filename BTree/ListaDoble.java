/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carlo
 */
public class ListaDoble {
    private NodoDoble Primero;
    private NodoDoble Ultimo;
    public int Cuenta;

    public ListaDoble() {
        this.Cuenta=0;
        this.Primero=null;
        this.Ultimo=null;
    }
    public boolean estaVacio(){
        return (Primero==null);
    }
    public void InsertarNodoD(String codigo,String pais){
        NodoDoble nuevo=new NodoDoble(codigo,pais);
        if(Cuenta<4){
            if(estaVacio()){
                Primero=nuevo;
                Ultimo=Primero;
            }
            else {
                Ultimo.setSiguiente(nuevo);
                nuevo.setAnterior(Ultimo);
                Ultimo=nuevo;
            }
            Cuenta++;
        }
        else {
            System.out.println("Ya se ha superado el tamaño");
        }
        
    }
    
    public void InsertarDato(String codigo,int posicion){
        NodoDoble aux=Primero;
        while(posicion!=0){
            posicion--;
            aux=aux.getSiguiente();
        }
        aux.setCodigo(codigo);
    }
    
    public NodoDoble DevolverDato(int posicion){
        NodoDoble aux=Primero;
        while(posicion!=0){
            posicion--;
            aux=aux.getSiguiente();
        }
        return aux;
    }
    public void MostrarDatos(){
        NodoDoble aux=Primero;
        while(aux!=null){
            System.out.println("Dato " + aux.getCodigo());
            aux=aux.getSiguiente();
        }
    }
}
