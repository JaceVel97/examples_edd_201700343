/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carlo
 */
public class ListaPuntero {
    private NodoPuntero Primero;
    private NodoPuntero Ultimo;
    private int Cuenta;

    public ListaPuntero() {
        Primero=null;
        Ultimo=null;
        Cuenta=0;
    }
    
    public boolean estaVacio(){
        return (Primero==null);
    }
    
    public void InsertarPuntero(PaginaB puntero){
        NodoPuntero nuevo=new NodoPuntero(puntero);
        if(Cuenta<5){
            if(estaVacio()){
                Primero=nuevo;
                Ultimo=Primero;
            }
            else {
                Ultimo.setSiguienteP(nuevo);
                nuevo.setAnteriorP(Ultimo);
                Ultimo=nuevo;
            }
            Cuenta++;
        }
        
    }
    public void InsertarPunteroP(PaginaB pagina,int posicion){
        NodoPuntero aux=Primero;
        while(posicion!=0){
            posicion--;
            aux=aux.getSiguienteP();
        }
        aux.setPuntero(pagina);
    }
    
    public NodoPuntero DevolverPuntero(int posicion){
        NodoPuntero aux=Primero;
        while(posicion!=0){
            posicion--;
            aux=aux.getSiguienteP();
        }
        return aux;
    }
    
    public NodoPuntero getPrimero() {
        return Primero;
    }

    public void setPrimero(NodoPuntero Primero) {
        this.Primero = Primero;
    }

    public int getCuenta() {
        return Cuenta;
    }

    public void setCuenta(int Cuenta) {
        this.Cuenta = Cuenta;
    }
    
}
