from HashNode import Node
import SolovayStrassen as ss
from cryptography.fernet import Fernet


class TablaHash:
    def __init__(self):
        self.table_size = 7
        self.hash_list = [None, None, None, None, None, None, None]

        key = Fernet.generate_key()
        self.f = Fernet(key)

        # Initialize the table with empty data
        for i in range(self.table_size):
            new_node = Node("", "", "")
            self.hash_list[i] = new_node

    def get_table(self):
        for i in range(self.table_size):
            print(str(i) + ") " + self.hash_list[i].code + " - " + str(self.hash_list[i].name) + " - " + self.hash_list[i].rating)

    def get_table_decrypt(self):
        for i in range(self.table_size):
            if self.hash_list[i].name != "":
                name_decrypt = self.f.decrypt(self.hash_list[i].name).decode()
            else:
                name_decrypt = self.hash_list[i].name
            print(str(i) + ") " + self.hash_list[i].code + " - " + name_decrypt + " - " + self.hash_list[i].rating)

    def insert_value(self, code, name, rating):
        position = self.get_position_table(int(code))
        if self.get_percent_use() <= 0.40:
            if self.hash_list[position].code == "":
                new_name = self.f.encrypt(name.encode())
                new_node = Node(code, new_name, rating)
                self.hash_list[position] = new_node
            else:
                position_initial = position
                while True:
                    if self.hash_list[position].code == "":
                        new_name = self.f.encrypt(name.encode())
                        new_node = Node(code, new_name, rating)
                        self.hash_list[position] = new_node
                        break
                    if position < self.table_size - 1:
                        position += 1
                    else:
                        position = 0
        else:
            for i in range(self.get_next_prime(self.table_size) - self.table_size):
                new_node = Node("", "", "")
                self.hash_list.append(new_node)

            self.table_size = self.get_next_prime(self.table_size)
            if self.hash_list[position].code == "":
                new_name = self.f.encrypt(name.encode())
                new_node = Node(code, new_name, rating)
                self.hash_list[position] = new_node
            else:
                while True:
                    if self.hash_list[position].code == "":
                        new_name = self.f.encrypt(name.encode())
                        new_node = Node(code, new_name, rating)
                        self.hash_list[position] = new_node
                        break
                    if position < self.table_size:
                        position += 1
                    else:
                        position = 0

    def get_position_table(self, code):
        return code % self.table_size

    def get_next_prime(self, initial_size):
        return ss.next_prime(initial_size)

    def get_percent_use(self):
        counter = 0
        for i in range(self.table_size):
            if self.hash_list[i].code != "":
                counter += 1

        return counter/self.table_size

