from Node import Node


class AVL:
    def __init__(self):
        self.Root = None

    def MAX(self, val1, val2):
        if val1 > val2:
            return val1
        else:
            return val2

    def insert(self, value):
        self.Root = self.insert_intern(value, self.Root)

    def height(self, node):
        if node is not None:
            return node.height
        return -1

    def insert_intern(self, value, root):
        if root is None:
            return Node(value)
        else:
            if value < root.number:
                root.left = self.insert_intern(value, root.left)
                if self.height(root.right) - self.height(root.left) == -2:
                    if value < root.left.number:
                        root = self.RI(root)
                    else:
                        root = self.RDI(root)
            elif value > root.number:
                root.right = self.insert_intern(value, root.right)
                if self.height(root.right) - self.height(root.left) == 2:
                    if value > root.left.number:
                        root = self.RD(root)
                    else:
                        root = self.RID(root)
            else:
                root.number = value

        root.height = self.MAX(self.height(root.left), self.height(root.right)) + 1
        return root

    def RI(self, node):
        aux = node.left
        node.left = aux.right
        aux.right = node
        node.height = self.MAX(self.height(node.left), self.height(node.right)) + 1
        aux.height = self.MAX(self.height(aux.left), self.height(aux.right)) + 1
        return aux

    def RD(self, node):
        aux = node.right
        node.right = aux.left
        aux.left = node
        node.height = self.MAX(self.height(node.left), self.height(node.right)) + 1
        aux.height = self.MAX(self.height(aux.left), self.height(aux.right)) + 1
        return aux

    def RDI(self, node):
        node.left = self.RD(node.left)
        return self.RI(node)

    def RID(self, node):
        node.right = self.RI(node.right)
        return self.RD(node)