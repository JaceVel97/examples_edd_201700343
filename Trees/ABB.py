from Node import Node


class ABB:
    def __init__(self):
        self.Root = None

    def insert(self, character, power):
        self.Root = self.insert_intern(character, power, self.Root)

    def insert_intern(self, character, power, root):
        if root is None:
            root = Node(character, power)
        else:
            if power > root.power:
                root.right = self.insert_intern(character, power, root.right)
            else:
                root.left = self.insert_intern(character, power, root.left)
        return root

    def pre_order(self):
        self.pre_order_intern(self.Root)

    def pre_order_intern(self, root):
        if root is not None:
            print("->Power: " + str(root.power) + " - Character: " + str(root.character))
            self.pre_order_intern(root.left)
            self.pre_order_intern(root.right)

    def in_order(self):
        self.in_order_intern(self.Root)

    def in_order_intern(self, root):
        if root is not None:
            self.in_order_intern(root.left)
            print("->Power: " + str(root.power) + " - Character: " + str(root.character))
            self.in_order_intern(root.right)

    def post_order(self):
        self.post_order_intern(self.Root)

    def post_order_intern(self, root):
        if root is not None:
            self.post_order_intern(root.left)
            self.post_order_intern(root.right)
            print("->Power: " + str(root.power) + " - Character: " + str(root.character))
