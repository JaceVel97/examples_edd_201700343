from flask import Flask, request, jsonify
from ABB_2 import BST

app = Flask(__name__)

abb_tree = BST()


@app.route('/value', methods=['GET'])
def hello_world():
    list_2 = abb_tree.in_orden()
    response = ""
    for number in list_2:
        response += number + "-"
    return jsonify({"response": response})


@app.route('/insert/value', methods=['POST'])
def insert_value():
    data = request.get_json(force=True)
    valor = data['number']
    abb_tree.insert_value(valor)
    return jsonify({"response": "Todo se recibio correctamente"})


if __name__ == "__main__":
    app.run("localhost", port=7000)
