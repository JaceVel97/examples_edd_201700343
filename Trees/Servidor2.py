from flask import Flask, request, jsonify

app = Flask(__name__)

app.route('/hello2', methods=["GET"])
def get_hello():
    return jsonify("Hello world")


if __name__ == "__main__":
    app.run("localhost", port=5000)