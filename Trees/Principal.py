from ABB import ABB
from ABB_2 import BST
from AVL import AVL
abb_tree = BST()

abb_tree.insert_value(1)
abb_tree.insert_value(2)
abb_tree.insert_value(3)
abb_tree.insert_value(4)
abb_tree.insert_value(5)
abb_tree.insert_value(6)

abb_tree.pre_orden()

print('------------------------')

avl_tree = AVL()

avl_tree.insert(1)
avl_tree.insert(2)
avl_tree.insert(3)
avl_tree.insert(4)
avl_tree.insert(5)
avl_tree.insert(6)

avl_tree.pre_orden()