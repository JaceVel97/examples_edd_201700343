from Node_2 import NodeABB


class BST:
    def __init__(self):
        self.Root = None

    def insert_value(self, number):
        self.Root = self.insert_value_intern(number, self.Root)

    def insert_value_intern(self, number, root):
        if root is None:
            root = NodeABB(number)
        else:
            if number > root.number:
                root.right = self.insert_value_intern(number, root.right)
            else:
                root.left = self.insert_value_intern(number, root.left)
        return root

    def pre_orden(self):
        self.pre_orden_intern(self.Root)

    def pre_orden_intern(self, root):
        if root is not None:
            print(root.number)
            self.pre_orden_intern(root.left)
            self.pre_orden_intern(root.right)

    def in_orden(self):
        list_ = []
        self.in_orden_intern(self.Root, list_)
        return list_

    def in_orden_intern(self, root, list_):
        if root is not None:
            self.in_orden_intern(root.left, list_)
            list_.append(root.number)
            self.in_orden_intern(root.right, list_)

    def post_orden(self):
        self.post_orden_intern(self.Root)

    def post_orden_intern(self, root):
        if root is not None:
            self.post_orden_intern(root.left)
            self.post_orden_intern(root.right)
            print(root.number)