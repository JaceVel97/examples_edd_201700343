from Node import Node


class AVL:
    def __init__(self):
        self.Root = None

    def MAX(self, val1, val2):
        if val1 > val2:
            return val1
        else:
            return val2

    def insert(self, value):
        self.Root = self.insert_intern(value, self.Root)

    def insert_intern(self, value, root):
        if root is None:
            return Node(value)
        else:
            if value < root.number:
                root.left = self.insert_intern(value, root.left)
                if self.height(root.right) - self.height(root.left) == -2:
                    if value < root.left.number:
                        root = self.RI(root)
                    else:
                        root = self.RDI(root)
            elif value > root.number:
                root.right = self.insert_intern(value, root.right)
                if self.height(root.right) - self.height(root.left) == 2:
                    if value > root.right.number:
                        root = self.RD(root)
                    else:
                        root = self.RDD(root)
            else:
                root.number = value

        root.height = self.MAX(self.height(root.left), self.height(root.right)) + 1
        return root

    def height(self, node):
        if node is not None:
            return node.height
        return -1

    def RI(self, node):
        aux = node.left
        node.left = aux.right
        aux.right = node
        node.height = self.MAX(self.height(node.right), self.height(node.left)) + 1
        aux.height = self.MAX(self.height(aux.left), node.height) + 1
        return aux

    def RD(self, node):
        aux = node.right
        node.right = aux.left
        aux.left = node
        node.height = self.MAX(self.height(node.right), self.height(node.left)) + 1
        aux.height = self.MAX(self.height(aux.right), node.height) + 1
        return aux

    def RDI(self, node):
        node.left = self.RD(node.left)
        return self.RI(node)

    def RDD(self, node):
        node.right = self.RI(node.right)
        return self.RD(node)

    def pre_orden(self):
        self.pre_orden_intern(self.Root)

    def pre_orden_intern(self, root):
        if root is not None:
            print(root.number)
            self.pre_orden_intern(root.left)
            self.pre_orden_intern(root.right)

    def in_orden(self):
        self.in_orden_intern(self.Root)

    def in_orden_intern(self, root):
        if root is not None:
            self.in_orden_intern(root.left)
            print(root.number)
            self.in_orden_intern(root.right)

    def post_orden(self):
        self.post_orden_intern(self.Root)

    def post_orden_intern(self, root):
        if root is not None:
            self.post_orden_intern(root.left)
            self.post_orden_intern(root.right)
            print(root.number)