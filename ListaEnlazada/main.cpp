#include <iostream>
#include "list.h"

using namespace std;

int main()
{
    DoubleList *newList = new DoubleList();
    newList->insertList(2);
    newList->insertList(7);
    newList->insertList(5);
    newList->insertList(3);
    cout<<"Lista normal"<<endl;
    newList->getList();
    cout<<"Borrando 7"<<endl;
    newList->deleteValue(7);
    newList->getList();
    cout<<"Lista inversa"<<endl;
    newList->getListReverse();
    cout<<"Cantidad: "<<newList->getSize()<<endl;
}