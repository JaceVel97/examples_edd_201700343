#include "simplelist.h"

ListS::ListS(){
    this->First=NULL;
    this->Last = NULL;
}

bool ListS::isEmpty(){
    return this->First == NULL;
}

int ListS::getSize(){
    Node *aux = this->First;
    int counter = 0;
    while(aux != NULL){
        counter++;
        aux = aux->getNext();
    }
    return counter;
}

void ListS::getListDouble(){
    Node *aux = this->First;
    while(aux != NULL){
        cout<<"Valor: "<<aux->getValue()<<endl;
        aux = aux->getNext();
    }
}


void ListS::insertValue(int value_){
    Node *newNode = new Node(value_);
    if(isEmpty()){
        this->First = newNode;
        this->Last = this->First;
    }
    else{
        this->Last->setNext(newNode);
        newNode->setPrevious(this->Last);
        this->Last = newNode;
    }
}