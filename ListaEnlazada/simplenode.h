#include <iostream>
#include <string>

using namespace std;

class Node{
private:
    int Value;
    Node *Next;
    Node *Previous;

public:
    //Constructs
    Node();
    Node(int value_);
    Node(int value_, Node *next_, Node *previous_);

    //Getters
    int getValue();
    Node *getNext();
    Node *getPrevious();

    //Setter
    void setValue(int value_);
    void setNext(Node *next_);
    void setPrevious(Node *previous_);
};