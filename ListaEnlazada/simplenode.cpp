#include "simplenode.h"

//Constructs
Node::Node(){
    this->Value = 0;
    this->Next =  NULL;
    this->Previous = NULL;
}

Node::Node(int value_){
    this->Value = value_;
    this->Next = 0;
    this->Previous = 0;
}

Node::Node(int value_, Node *next_, Node *previous_){
    this->Value = value_;
    this->Next = next_;
    this->Previous = previous_;
}

//Getters
int Node::getValue(){
    return this->Value;
}

Node *Node::getNext(){
    return this->Next;
}

Node *Node::getPrevious(){
    return this->Previous;
}
//Setters
void Node::setValue(int value_){
    this->Value = value_;
}

void Node::setNext(Node *next_){
    this->Next = next_;
}
void Node::setNext(Node *previous_){
    this->Next = previous_;
}
