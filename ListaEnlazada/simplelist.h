#include "simplenode.h"

class ListS{
private:
    Node *First;
    Node *Last;

public:
    //Construct
    ListS();

    //Methods
    bool isEmpty();
    int getSize();

    void getListDouble();
    void getListReverse();

    void insertValue(int value);

    void deleteValue(int value);
};
