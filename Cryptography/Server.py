from flask import Flask, request, jsonify
from Data import NodeData
from cryptography.fernet import Fernet
import json
import requests

key = Fernet.generate_key()
f = Fernet(key)

app = Flask(__name__)

list_data = []


@app.route('/', methods=['GET'])
def hello_world():
    return jsonify({"response": "Todo esta genial y funcionando"})


@app.route('/getData', methods=['GET'])
def getData():
    result = "[\n"
    for i in range(len(list_data)):
        result += '{\n"name": "' + list_data[i].name + '",\n'
        result += '\n"email": "' + list_data[i].email + '",\n'
        result += '\n"password": "' + list_data[i].password + '",\n'
        result += '\n"age": "' + list_data[i].age + '",\n'
        if i == (len(list_data) - 1):
            result += '\n"phone": "' + list_data[i].phone + '"\n}'
        else:
            result += '\n"phone": "' + list_data[i].phone + '"\n},'
    result += "\n]"
    return jsonify(json.loads(result))


@app.route('/getDataD', methods=['GET'])
def getDataD():
    result = "[\n"
    for i in range(len(list_data)):
        result += '{\n"name": "' + list_data[i].name + '",\n'
        result += '\n"username": "' + list_data[i].email + '",\n'
        result += '\n"password": "' + f.decrypt(list_data[i].password.encode()).decode() + '",\n'
        result += '\n"longitud": "' + list_data[i].age + '",\n'
        if i == (len(list_data) - 1):
            result += '\n"latitud": "' + list_data[i].phone + '"\n}'
        else:
            result += '\n"latitud": "' + list_data[i].phone + '"\n},'
    result += "\n]"
    return jsonify(json.loads(result))


@app.route('/login', methods=['POST'])
def login():
    data = request.get_json(force=True)
    result = ""
    print(data['username'])
    print(data['password'])
    for i in range(len(list_data)):
        if data['username'] == list_data[i].email and data['password'] == f.decrypt(list_data[i].password.encode()).decode():
            result += '{\n"name": "' + list_data[i].name + '",\n'
            result += '\n"username": "' + list_data[i].email + '",\n'
            result += '\n"password": "' + f.decrypt(list_data[i].password.encode()).decode() + '",\n'
            result += '\n"longitud": "' + list_data[i].age + '",\n'
            result += '\n"latitud": "' + list_data[i].phone + '"\n}'
            return jsonify(json.loads(result))
    result = '{\n"name": "",\n'
    result += '\n"username": "",\n'
    result += '\n"password": "",\n'
    result += '\n"longitud": "",\n'
    result += '\n"latitud": ""\n}'
    return jsonify(json.loads(result))


@app.route('/register', methods=['POST'])
def insertUser():
    data = request.get_json(force=True)
    new_node = NodeData(data['name'], data['username'], f.encrypt(str(data['password']).encode()).decode(), data['longitud'], data['latitud'])
    list_data.append(new_node)
    return jsonify({"response": "Datos insertados correctamente"})

@app.route('/currentweather', methods=['POST'])
def get_current():
    #session = Session()
    #luser = session.query(User).filter(User.username==request.get_json()['username'])
    #esto servira para escribir un log de la consulta
    userdata = request.get_json()
    api_key = "033543edb64c3da524b3a8bedf52c37c"
    lat = "14.618901"
    lon = "-90.500116"
    units = "metric"
    lang = "es"
    url = "https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s&units=%s&lang=%s" % (lat, lon, api_key, units, lang)

    response = requests.get(url)
    data = json.loads(response.text)
    return data, 201


if __name__ == "__main__":
    app.run("localhost", port=7000)