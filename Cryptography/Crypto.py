from cryptography.fernet import Fernet
import random
key = Fernet.generate_key()

print(type(key))

f = Fernet(key)

token = f.encrypt(b"Este es mi mayor secreto.")

print(token)
print(type(token))

result = f.decrypt(token)

print(result)
print(type(result))

for i in range(25):
    print("PR_M" + str(random.randint(1, 50)))
