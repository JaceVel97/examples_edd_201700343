class NodeIntern:
    def __init__(self, Curso, Creditos, Up = None, Down = None):
        self.Curso = Curso
        self.Creditos = Creditos
        self.Up = Up
        self.Down = Down

    # Getters
    def getCurso(self):
        return self.Curso

    def getCreditos(self):
        return self.Creditos

    def getUp(self):
        return self.Up

    def getDown(self):
        return self.Down

    # Setters

    def setCurso(self, curso):
        self.Curso = curso

    def setCreditos(self, creditos):
        self.Creditos = creditos

    def setUp(self, up):
        self.Up = up

    def setDown(self, down):
        self.Down = down