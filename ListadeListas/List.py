from NodeIntern import NodeIntern
from NodeList import NodeList

class List:
    def __init__(self):
        self.First = None
        self.Last = None

    def getSize(self):
        aux = self.First
        counter = 0
        while aux is not None:
            counter += 1
            aux = aux.get_next()

        return counter

    def isEmpty(self):
        return self.First is None

    def getList(self):
        aux = self.First
        while aux is not None:
            print(aux.getNombre() + " - " + aux.getCarnet())
            aux = aux.get_next()

    def getListCourse(self):
        aux = self.First
        while aux is not None:
            print(aux.getNombre() + " - " + aux.getCarnet())

            aux2 = self.First.getDown()
            while aux2 is not None:
                print(aux2.getCurso() + " - " + aux.getCreditos())
                aux2 = aux2.getDown()

            aux = aux.get_next()

    def insertValue(self, Nombre, Carnet):
        new_node = NodeList(Nombre, Carnet)

        if self.isEmpty():
            self.Last = new_node
            self.First = self.Last
        else:
            self.Last.setNext(new_node)
            new_node.setPrevious(self.Last)
            self.Last = new_node

    def insertValueIntern(self, Carnet, Curso, Creditos):
        aux = self.First
        verification = False
        while aux is not None:
            if self.First.getCarnet() == Carnet:
                verification = True
                new_node = NodeIntern(Curso, Creditos)
                if aux.getDown() is None:
                    self.First.setDown(new_node)
                else:
                    aux2 = self.First.getDown()
                    while(aux2.getDown().getDown() is not None):
                        self.First.setDown(self.First.getDown().getDown())

                    self.First.getDown().setDown(new_node)
                    self.First.setDown(aux2)
        if verification is False:
            print("No se encontro el carnet")
            aux = aux.get_next()



