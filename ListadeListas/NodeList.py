class NodeList:
    def __init__(self, Nombre, Carnet, Next = None, Previous = None, Down = None):
        self.Nombre = Nombre
        self.Carnet = Carnet
        self.Next = Next
        self.Previous = Previous
        self.Down = Down

    # Getters
    def getNombre(self):
        return self.Nombre

    def getCarnet(self):
        return self.Carnet

    def getNext(self):
        return self.Next

    def getPrevios(self):
        return self.Previous

    def getDown(self):
        return self.Down

    # Setters

    def setNombre(self, nombre):
        self.Nombre = nombre

    def setCarnet(self, carnet):
        self.Carnet = carnet

    def setNext(self, next):
        self.Next = next

    def setPrevious(self, previous):
        self.Previous = previous

    def setDown(self, down):
        self.Down = down